<!--
SPDX-FileCopyrightText: 2021 Huawei Inc.
SPDX-License-Identifier: Apache-2.0
-->

# Bitbake build container for Open Harmony OSTC

This container encapsulates build dependencies necessary to use repo and bitbake
for Open Harmony from
https://gitlab.eclipse.org/eclipse/oniro-core/meta-openharmony/

The default user is called builder, making usage of bitbake easier.
